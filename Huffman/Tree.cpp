#include "Tree.hpp"
#include <QtAlgorithms>
#include "Compression.hpp"

Tree::Tree(quint64 *vetor)
{
    buildList(vetor);
    m_vector = new QString[256];
}


void Tree::buildList(quint64 *vetor)
{
    for(int count = 0; count < 256; ++count){
        if (vetor[count]){
            Node *temp = new Node(vetor[count], count);
            m_list.append(temp); //adicionando os caracteres do arquivo na lista
        }
    }
    qSort(m_list.begin(), m_list.end(), Compression::comp);
}

void Tree::buildTree()
{
    Q_ASSERT_X(m_list.length() >= 2, "buildTree()", "Can't build.");
    while(m_list.length() > 2){ //pegando os dois menores para criar o nó e ordenar
        qSort(m_list.begin(), m_list.end(), Compression::comp);
        Node* temp = new Node(m_list.at(1),m_list.at(0));
        m_list.removeFirst();
        m_list.removeFirst();
        m_list.insert(0, temp);
    }
    if(m_list.length() == 2) { //montar a árvore
        m_root = new Node(m_list.at(1),m_list.at(0));
        m_list.removeFirst();
        m_list.removeFirst();
        m_list.insert(0,m_root);
    }
}

void Tree::buildCoding(Node *aNode, QString temp)
{
    Q_ASSERT_X(m_root,"buildCoding()", "Sorry. Not possible.");
    if(aNode->isLeaf()) {
        m_vector[aNode->m_symbol] = temp; //se for folha, guardar a codificação
    } else { //descer pra esquerda e depois voltar e descer pra direita
        temp += '0';
        buildCoding(aNode->getLeft(), temp);
        temp = temp.mid(0, temp.length() - 1);
        temp += '1';
        buildCoding(aNode->getRight(), temp);
    }
}


QString *Tree::getVector() const
{
    return m_vector;
}

void Tree::treeRep(Node *aNode)
{
    if (aNode) {
        if (aNode->isLeaf()){
            if((aNode->m_symbol == '*') || (aNode->m_symbol == '!')) {
                m_representation += '!'; //marcador
            }
            m_representation += aNode->m_symbol;
        }
        else {
            m_representation += '*';
            treeRep(aNode->getLeft());
            treeRep(aNode->getRight());
        }
    }
}

QByteArray Tree::getRep()
{
    return m_representation;
}

