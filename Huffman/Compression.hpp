#ifndef COMPRESSION_H
#define COMPRESSION_H
#include "Tree.hpp"

#include <QObject>
class Compression : public QObject
{
    Q_OBJECT
public:
    Compression();

    //
    int sizeTrash;
    QByteArray bitString;
    QByteArray byteString;
    //
    Compression(QString string1,QString string2 = "");
    Q_INVOKABLE static void GUICompress(QUrl s1, QUrl s2); //compressão pela interface
    bool readFile(); //leitura do arquivo
    quint64 *getFrequency() const; //pegar a frequência dos caracteres

    static bool comp(Node*left, Node*right);

    void generateBin(QString *vetor); //gerador de lixo

    void buildHeader(QByteArray rep); //construir o header

    QByteArray bitToByte(QString &string); //bitToByte recebendo string

    QByteArray bitToByte(QByteArray &bytearray); //bitTOByte recebendo ByteArray

    QString getPath(); //pegar o caminho

    void writeFileHuff(); //escrever .huff

private:
    QString filePath;
    QString fileOut;
    quint64 *m_frequency;
    QByteArray m_header;
//


};

#endif // COMPRESSION_H
