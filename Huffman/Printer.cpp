#include "Printer.hpp"
#include "Compression.hpp"

void Printer::printRepetition(const quint64 *frequencia)
{
    qDebug() << "Number:\t" << "Symbol:\t" << "Repetition:\t";
    for(int count = 0; count < 256; ++count){
        if (frequencia[count]){
            qDebug() << count << "\t" << qPrintable(QString(count)) << "\t" << frequencia[count];
        }
    }
}

void Printer::printTree(Node *aNode, int level)
{
    if(aNode != 0){
        printTree(aNode->getRight(), level + 1);
        if(aNode->isLeaf())
            qDebug() << qPrintable(QString("\t").repeated(level))
                     << char(aNode->m_symbol) << "/" << aNode->m_frequency;
        else
            qDebug() << qPrintable(QString("\t").repeated(level)) << char(46);
        printTree(aNode->getLeft(), level + 1);
    }
}

void Printer::printCoding(QString *vetor)
{
    qDebug() << "Number:\t" << "Symbol:\t" << "Coding:\t";
    for(int count = 0; count < 256; ++count){
        if (vetor[count] != ""){
            qDebug() << count << "\t" << qPrintable(QString(count)) << "\t" << vetor[count];
        }
    }
}

void Printer::printTreeRep(QByteArray representacao)
{
    qDebug() << qPrintable(representacao);
}

