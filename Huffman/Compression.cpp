#include "Compression.hpp"

Compression::Compression()
{

}

Compression::Compression(QString string1, QString string2)
{
    filePath = string1;
    fileOut = string2;
    m_frequency = new quint64[256];
    for (int i = 0; i < 256; ++i){
        m_frequency[i] = 0;  //para pegar o arquivo corretamente
    }

}

bool Compression::readFile()
{
    QFile file(filePath);

    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error!" << Q_FUNC_INFO << file.errorString();
        return false; //confere se o arquivo existe
    }

    while(!file.atEnd()) {
        QByteArray line = file.read(1024);
        for (int count = 0; count < line.size(); ++count) {
            ++m_frequency[(uchar)line[count]]; //conta a frequência
        }
    }
    file.close();
    return true;
}

quint64 *Compression::getFrequency() const
{
    return m_frequency;
}

bool Compression::comp(Node *left, Node *right)
{
    if(left->m_frequency != right->m_frequency) {
        return left->m_frequency < right->m_frequency;
    }
    return right->m_symbol < left->m_symbol;
}

void Compression::generateBin(QString *vetor)
{
    QFile file(filePath);
    file.open(QIODevice::ReadOnly);

    while(!file.atEnd()) {
        QByteArray line = file.read(1024);
        for (int count = 0; count < line.size(); ++count) {
            bitString.append(vetor[(uchar)line[count]]); //lendo os caracteres e escrevendo ele codificado
        }                                                //e escrevendo codificado na bitString

        while(bitString.size() >= 8) {
            byteString += bitToByte(bitString);
            bitString.remove(0, 8); //passando o codificado pra byte
        }
    }

    if(bitString.size() & 7) { //pegando o lixo
        sizeTrash = 8 - bitString.size();
        for(int count = 0; count < sizeTrash; ++count) {
            bitString.append('0');
        }

        byteString += bitToByte(bitString);
        bitString.remove(0, 8);
    }
    file.close();
}

void Compression::buildHeader(QByteArray rep)
{
    QString aux = QString::number(sizeTrash, 2);
    //pegando o tamanho do lixo:
    if(aux.size() < 3) aux.prepend('0').repeated(3 - aux.size());

    QString aux2 = QString::number(rep.size(), 2);
    //pegando os 13 bits do tamanho da árvore
    if(aux2.size() < 13) aux2.prepend('0').repeated(13 - aux2.size());

    aux += aux2;

    aux2 = QString::number(QFileInfo(filePath).fileName().size(), 2);
    //pegando os 8 bits do tamanho do nome
    if(aux2.size() < 8) aux2.prepend('0').repeated(8 - aux2.size());

    aux += aux2;

    m_header = bitToByte(aux);
    m_header.append(QFileInfo(filePath).fileName()); //adicionando o nome
    m_header += rep;
}

QByteArray Compression::bitToByte(QString &string) {
    QByteArray byteAux;
    while(!string.isEmpty()) {
        QString byte = string.left(8);
        string.remove(0, 8); //removendo a partir da posição 0 da string 8 caracteres
        char aux = byte.toInt(0, 2);
        byteAux.append(aux);
    }
    return byteAux;
}

QByteArray Compression::bitToByte(QByteArray &bytearray) {
    QByteArray byteAux;
    while(bytearray.size()){
        QByteArray byte = bytearray.left(8);
        bytearray.remove(0, 8);
        char aux = byte.toInt(0, 2);
        byteAux.append(aux);
    }
    return byteAux;
}

QString Compression::getPath()
{
    return filePath;
}

void Compression::writeFileHuff()
{
    if(fileOut == "") {
        fileOut = QFileInfo(filePath).fileName();
    } if(fileOut.contains('.')) {
        fileOut.chop(fileOut.length() - fileOut.lastIndexOf('.'));
    }
    fileOut.append(".huff");
    QString aux;
    aux.append(QFileInfo(filePath).path()).append('/');
    fileOut.prepend(aux);

    QFile file(fileOut);
    file.open(QIODevice::WriteOnly);
    file.write(m_header);
    file.write(byteString);
    file.close();
}

void Compression::GUICompress(QUrl s1, QUrl s2) {
    QString input = s1.toLocalFile();
    QString output = s2.toString();

    QTime timer;
    timer.start();

    qDebug() << output;

    Compression *encode = new Compression(input, output);
    encode->readFile();
    Tree *arvore = new Tree(encode->getFrequency());
    arvore->buildTree();
    arvore->buildCoding(arvore->getRoot());
    arvore->treeRep(arvore->getRoot());
    encode->generateBin(arvore->getVector());
    encode->buildHeader(arvore->getRep());
    encode->writeFileHuff();

    qDebug("Elapsed time: %d\n", timer.elapsed());
}
