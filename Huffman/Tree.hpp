#ifndef TREE_H
#define TREE_H
#include "Node.hpp"
#include <QtAlgorithms>

class Tree
{
public:
    Tree(quint64 *vetor);

    void buildList(quint64 *vetor);

    void buildTree();

    void buildCoding(Node* aNode,QString temp = "");

    Node * getRoot() const{
        return this->m_root;
    }

    QString *getVector() const; //retorna a codificação

    void treeRep(Node* aNode); //representação da árvore

    QByteArray getRep();
//
private:
    Node *m_root;
    QList <Node*> m_list;
    QString *m_vector;
    QByteArray m_representation;
    //QByteArray m_header;
};

#endif // TREE_H
