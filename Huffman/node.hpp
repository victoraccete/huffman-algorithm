#ifndef NODE_H
#define NODE_H
#include <QtCore>

class Node
{
public:
    const quint64 m_frequency;
    const uchar m_symbol;

    Node(quint64 frequency, uchar symbol): m_frequency(frequency),m_symbol(symbol),
        m_left(NULL),m_right(NULL){}

    Node(Node*left,Node*right): m_left(left),m_right(right),
    m_frequency(left->m_frequency + right->m_frequency),m_symbol(0){}

    ~Node(){
        delete m_left;
        delete m_right;
    }


    bool isLeaf(){
        if (!m_left && !m_right) return true;
        return false;
    }

    Node * getRight() const {
        return this->m_right;
    }

    Node * getLeft() const {
        return this->m_left;
    }

private:
    Node *m_right;
    Node *m_left;

};

#endif // NODE_H
