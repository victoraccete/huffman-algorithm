TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    Compression.cpp \
    Printer.cpp \
    Tree.cpp

RESOURCES += qml.qrc

RC_ICONS = hufflogo4.ico

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    Compression.hpp \
    Node.hpp \
    Printer.hpp \
    Tree.hpp

