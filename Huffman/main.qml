import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Window {
    visible: true
    height: 350
    width: 275
    minimumHeight: 350
    minimumWidth: 250
    maximumHeight: 450
    maximumWidth: 325
    title: qsTr("Huffman")

    Rectangle {
        id: background
        anchors.fill: parent
        color: "#CC0066"
    }
    Rectangle {
        id: main
        height: background.height*0.98
        width: background.width*0.98
        color: "white"
        radius: 10
        anchors
        {
            horizontalCenter: background.horizontalCenter
            verticalCenter: background.verticalCenter
        }
        Rectangle{
            id: auxiliar
            height: main.height*0.40
            width: main.width*0.65
            color: "white"
            border.color:
            {
                width: 1
                color: "white"
            }
            anchors
            {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
            Text{
                id: title
                text: "Huffman"
                anchors{
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.top
                }
                font.pointSize: 24; font.bold: true; color: "#339999"
            }

            ColumnLayout{
                id: inputColumn
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 12
                TextField {
                    id: textFileInput
                    placeholderText: qsTr("Input")
                    font.pointSize: 12
                    width: buttonRow.width
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Rectangle {
                    id: buttonBrowseIn
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width/2
                    anchors.horizontalCenter: inputColumn.horizontalCenter
                    radius: 30
                    Text{
                        id: browseInText
                        text: "Browse Input"
                        anchors{
                            horizontalCenter: buttonBrowseIn.horizontalCenter
                            verticalCenter: buttonBrowseIn.verticalCenter
                        }
                        font.pointSize: 10; font.bold: true; color: "#339999"

                        FileDialog
                        {
                            id: inputDialog
                            onAccepted:
                            {
                                textFileInput.text = inputDialog.fileUrl;
                            }
                        }

                        MouseArea
                        {
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: browseInText.color = "#CC0066"
                            onExited: browseInText.color = "#339999"
                            onClicked:
                            {
                                inputDialog.visible = !inputDialog.visible;
                            }
                        }
                    }
                }

                TextField {
                    id: textFileOutput
                    placeholderText: qsTr("Output")
                    font.pointSize: 12
                    width: buttonRow.width
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Rectangle {
                    id: buttonBrowseOut
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width/2
                    anchors.horizontalCenter: inputColumn.horizontalCenter
                    radius: 30
                    Text{
                        id: browseOutText
                        text: "Browse Output"
                        anchors{
                            horizontalCenter: buttonBrowseOut.horizontalCenter
                            verticalCenter: buttonBrowseOut.verticalCenter
                        }
                        font.pointSize: 10; font.bold: true; color: "#339999"

                        FileDialog
                        {
                            id: outputDialog
                            selectFolder: true
                            onAccepted:
                            {
                                textFileOutput.text = outputDialog.fileUrl;
                            }
                        }

                        MouseArea
                        {
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: browseOutText.color = "#CC0066"
                            onExited: browseOutText.color = "#339999"
                            onClicked:
                            {
                                outputDialog.visible = !outputDialog.visible;
                            }
                        }
                    }
                }
            }
//            FileDialog
//            {
//                id: inputDialog
//                onAccepted:
//                {
//                    textFileInput.text = inputDialog.fileUrl;
//                }
//            }
//            FileDialog
//            {
//                id: outputDialog
//                selectFolder: true
//                onAccepted:
//                {
//                    textFileOutput.text = outputDialog.fileUrl;
//                }
//            }

            RowLayout{
                id: buttonRow
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                height: auxiliar.height*0.25
                width: auxiliar.width
                spacing: 1
                Rectangle{
                    id: buttonCompress
                    color: "#339999"
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width/2
                    radius: 30
                    Text{
                        id: compressText
                        text: "Compress"
                        anchors{
                            horizontalCenter: buttonCompress.horizontalCenter
                            verticalCenter: buttonCompress.verticalCenter
                        }

                        font.pointSize: 10; font.bold: true; color: "white"
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: buttonCompress.color = "#CC0066"
                        onExited: buttonCompress.color = "#339999"
                        onClicked:
                        {
                            encode.GUICompress(textFileInput.text, textFileOutput.text);
                        }
                    }
                }
                Rectangle{
                    id: buttonDecompress
                    color: "#339999"
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width/2
                    radius: 30
                    Text{
                        id: deompressText
                        text: "Decompress"
                        anchors{
                            horizontalCenter: buttonDecompress.horizontalCenter
                            verticalCenter: buttonDecompress.verticalCenter
                        }

                        font.pointSize: 10; font.bold: true; color: "white"
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        hoverEnabled: true
                        onEntered: buttonDecompress.color = "#CC0066"
                        onExited: buttonDecompress.color = "#339999"
                        onClicked:
                        {
                            //função para fazer a descompressão
                        }
                    }
                }
            }
        }
    }
    Text{
        anchors.horizontalCenter: main.horizontalCenter
        anchors.bottom: main.bottom
        text: "v 0.3"
        color: "#101010"
        font.pointSize: 9
    }
}

