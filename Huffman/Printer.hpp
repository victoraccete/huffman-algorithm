#ifndef PRINTER_H
#define PRINTER_H
#include "Compression.hpp"


class Printer
{
public:
    static void printRepetition(quint64 const *frequencia);

    static void printTree(Node *aNode, int level = 0);

    static void printCoding(QString *vetor);

    static void printTreeRep(QByteArray representacao);
};

#endif // PRINTER_H
