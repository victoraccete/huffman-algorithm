#include <QCommandLineParser>
#include "Printer.hpp"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QGuiApplication::setApplicationName("Huffman Algorithm");
    QGuiApplication::setApplicationVersion("0.3");
    QCommandLineParser input;

    input.setApplicationDescription("Huffman Parsers");

    input.addHelpOption();

    input.addPositionalArgument("file",     QGuiApplication::translate("main", "File path"));
    input.addPositionalArgument("modifier", QGuiApplication::translate("main", "Input modifier."));

    QCommandLineOption option1("gui",   QGuiApplication::translate("main", "Start User Interface"));
    QCommandLineOption option2("c",     QGuiApplication::translate("main", "Start compression mode."));
    QCommandLineOption option3("o",     QGuiApplication::translate("main", "Rename output file."));
    QCommandLineOption option4("d",     QGuiApplication::translate("main", "Choose decompression directory"));

    input.addOption(option1);
    input.addOption(option2);
    input.addOption(option3);
    input.addOption(option4);

    input.process(app);

    if(input.isSet(option1) && app.arguments().count() == 2) {
        QQmlApplicationEngine engine;
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

        Compression encode;

        //Decompression decode;

        engine.rootContext()->setContextProperty("encode", &encode);
        //interpreter.setContextProperty("Decode", &decode);

        app.exec();
        //Encode
    } else if(input.isSet(option2)) {
        if(input.isSet(option3) && app.arguments().count() == 5) {
            QTime timer;
            timer.start();

            Compression *encode = new Compression(app.arguments().at(2), app.arguments().at(4));
            encode->readFile();
            Tree *arvore = new Tree(encode->getFrequency());
            arvore->buildTree();
            arvore->buildCoding(arvore->getRoot());
            arvore->treeRep(arvore->getRoot());
            encode->generateBin(arvore->getVector());
            encode->buildHeader(arvore->getRep());
            encode->writeFileHuff();

            qDebug("Time elapsed: %d\n", timer.elapsed());

        } else if(!(input.isSet(option3)) && app.arguments().count() == 3) {
            QTime timer;
            timer.start();

            Compression *encode = new Compression(app.arguments().at(2));
            encode->readFile();
            Tree *arvore = new Tree(encode->getFrequency());
            arvore->buildTree();
            arvore->buildCoding(arvore->getRoot());
            arvore->treeRep(arvore->getRoot());
            encode->generateBin(arvore->getVector());
            encode->buildHeader(arvore->getRep());
            encode->writeFileHuff();

            qDebug("Time elapsed: %d\n", timer.elapsed());
        }
    }

    return 0;
}

