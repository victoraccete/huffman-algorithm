#ifndef ENCODINGTREE_H
#define ENCODINGTREE_H
#include <QList>
#include <QHash>
#include "node.h"
#include "huffman.h"
#include <QtAlgorithms>
#include <QDebug>

class Tree
{
public:
    Tree();
    Tree(Node *root);
    ~Tree();
    QByteArray representation(Node *aNode);
    QByteArray formalize(Node *aNode);

    void printList();
    void buildList(Huffman* archive);
    void buildTreeHuff(Huffman* archive);
    void sorting();
    Node* getRoot();
    QHash<uchar,QString> getHash() const;
    QHash<uchar, QString> buildHash(Node* aNode,QString temp = "");

    void printTree(Node* aNode, int level = 0);
    void printHash(Tree*& m_tree);
    void printRepresentation(Tree*& m_tree);

    QByteArray getRepresentation() const;


    QHash<uchar,QString> m_hash;
    QList<Node*> m_list;

    Node* m_root;

private:
    QByteArray m_representation;
};

#endif // ENCODINGTREE_H
