#ifndef HUFFMAN_H
#define HUFFMAN_H
#include <QString>
#include <QFile>
#include <QByteArray>
#include <QIODevice>
#include <QObject>
#include "node.h"

class Huffman
{
public:
    Huffman();

    int *countTimes(QString directory);

    void printCharTimes(int *count);

    static bool comp(Node* left, Node*right);

    int* byteFrequency(QString directory);

    void setBitString(QString directory, QHash<uchar, QString> hash);

    int getTrash() const;


    int m_times[256];

private:
    int m_trash;
    QString bitString;

};

#endif // HUFFMAN_H
