#ifndef NODE_H
#define NODE_H

class Node{
public:
    int m_content;
    int m_symbol;
    Node * leftChild;
    Node * rightChild;

    Node(){
       m_content = 0;
       leftChild = 0;
       rightChild = 0;
    }

    Node(int m_content, int m_symbol, Node* left, Node* right){
        this->m_content = m_content;
        this->m_symbol = m_symbol;
        this->leftChild = left;
        this->rightChild = right;
    }


    int getContent() const{
        return this->m_content;
    }

    void setContent(const int &value) {
        this->m_content = value;
    }

    Node * getRight() const {
        return this->rightChild;
    }

    void setRight(Node * value){
        this->rightChild = value;
    }

    Node * getLeft() const {
        return this->leftChild;
    }

    void setLeft(Node * value){
        this->leftChild = value;
    }

    int getSymbol() const{
        return m_symbol;
    }

    bool isLeaf(){
        if((leftChild == 0) && (rightChild == 0))
            return true;
        return false;
    }
};

#endif // NODE_H

