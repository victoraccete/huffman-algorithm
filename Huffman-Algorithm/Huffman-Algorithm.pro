#-------------------------------------------------
#
# Project created by QtCreator 2015-05-09T22:40:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Huffman-Algorithm
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    huffman.cpp \
    encodingTree.cpp

HEADERS += \
    huffman.h \
    node.h \
    encodingTree.h
