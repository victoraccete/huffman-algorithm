#include <QDebug>
#include "huffman.h"
#include "encodingTree.h"
#include "node.h"

int main()
{
    Huffman *huff = new Huffman();
    Tree *arvore = new Tree();
    int *count = huff->countTimes("\\Users\\Victor\\Dropbox\\Teste.txt"); //escolhendo o arquivo para ler
    huff->byteFrequency("\\Users\\Victor\\Dropbox\\Teste.txt");

    arvore->buildTreeHuff(huff);
    arvore->buildHash(arvore->getRoot());
    arvore->representation(arvore->getRoot());

    huff->printCharTimes(count);
    huff->setBitString("\\Users\\Victor\\Dropbox\\Teste.txt", arvore->getHash());
    arvore->printList();

    arvore->printTree(arvore->m_root);
    arvore->printHash(arvore);
    arvore->printRepresentation(arvore);


    return 0;
}
