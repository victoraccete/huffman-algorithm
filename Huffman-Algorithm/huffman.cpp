#include "huffman.h"
#include <QDebug>

Huffman::Huffman(){
    for(int count = 0; count < 256; count++){
        m_times[count] = 0;
    }

   m_trash = 0;
   bitString = "";
}

int* Huffman::countTimes(QString directory){
    QFile *archive = new QFile(directory); //pegar o archive de um determinado diretório
    if(archive->open(QIODevice::ReadOnly)){ //apenas leitura
        QByteArray File = archive->readAll(); //guardando no File
        for(int aux = 0; aux < File.size(); aux++){ //contador
            m_times[uchar(File.at(aux))]++;
        }
    } else {
        return NULL; //o archive não existe
    }
    return m_times;
}

void Huffman::printCharTimes(int *count)
{
    for (int i = 0; i < 256; i++){
        if(count[i]){ //impressão formatada
            qDebug() << "ID na Tabela ASCII:" << i << "  Número de vezes:" << count[i] << "  Caractere" << char(i);
        }
    }
}

bool Huffman::comp(Node *left, Node *right)
{
    if(left->getContent() != right->getContent()) {
        return left->getContent() < right->getContent();
    }
    return right->getSymbol() < left->getSymbol();
}

int *Huffman::byteFrequency(QString directory)
{
    QFile* archive = new QFile(directory);
    Q_ASSERT_X(archive->open(QIODevice::ReadOnly), Q_FUNC_INFO, "File does not exist.");
    QByteArray binaryFile = archive->readAll();
    for(int count = 0; count != binaryFile.size(); count++){
        m_times[uchar(binaryFile.at(count))]++;
    }
    return m_times;
}

void Huffman::setBitString(QString directory, QHash<uchar, QString> hash)
{
    QFile* archive = new QFile(directory);
    archive->open(QIODevice::ReadOnly);
    QByteArray binaryFile = archive->readAll();

    for(int count = 0; count != binaryFile.size(); count++) {
        bitString += hash.value(uchar(binaryFile.at(count)));
    }

    if(bitString.size() % 8) {
        m_trash = 8 - (bitString.size() % 8);
    }

    bitString.append("0").repeated(m_trash);
}

int Huffman::getTrash() const
{
    return m_trash;
}



