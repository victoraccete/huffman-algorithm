#include "encodingTree.h"
#include "huffman.h"
#include "QtAlgorithms"

Tree::Tree()
{
    m_root = new Node();
}

Tree::Tree(Node* root)
{
    m_root = root;
}

Tree::~Tree()
{
    delete this;
}

QByteArray Tree::representation(Node *aNode)
{
    QByteArray myRep;
    if(aNode->isLeaf()) {
        if((aNode->getSymbol() == 0x21) || (aNode->getSymbol() == 0x2A)) {
            myRep.append(0x21);
        }
        myRep.append(aNode->getSymbol());
        return myRep;
    } else {
        myRep.append(0x2A);
        myRep += representation(aNode->getLeft()) + representation(aNode->getRight());
    }
    return myRep;
}

QByteArray Tree::formalize(Node *aNode)
{
    QByteArray end = representation(aNode);
    end.remove(0,1);
    return end;
}

void Tree::printList()
{
    for(int count = 0; count < m_list.size(); count++){
        qDebug() << "Caractere: " << m_list.at(count)->m_symbol
                 << "Repeticoes: " << m_list.at(count)->m_content;
    }
}

void Tree::buildList(Huffman *archive)
{
    for(int count = 0; count < 256; ++count) {
        if(archive->m_times[count]) {
            Node * temp = new Node(archive->m_times[count],count,NULL,NULL);
            m_list.append(temp);
        }
    }
    qSort(m_list.begin(), m_list.end(), Huffman::comp);
}

void Tree::buildTreeHuff(Huffman *archive)
{
    buildList(archive);
    Q_ASSERT_X(m_list.length() >= 2, "buildTree()", "Can't build.");
    while(m_list.length() > 2){
        qSort(m_list.begin(), m_list.end(), Huffman::comp);
        Node* temp = new Node(m_list.at(0)->getContent() + m_list.at(1)->getContent(),0 , m_list.at(0), m_list.at(1));
        m_list.removeFirst();
        m_list.removeFirst();
        m_list.insert(0, temp);
    }
    if(m_list.length() == 2) {
        m_root = new Node(m_list.at(0)->getContent()
                          + m_list.at(1)->getContent(),0 ,
                                 m_list.at(0), m_list.at(1));
        m_list.removeFirst();
        m_list.removeFirst();
        m_list.insert(0,m_root);
        qDebug() << "Tree built.";
    }
}

Node *Tree::getRoot()
{
    return m_root;
}

QHash<uchar, QString> Tree::getHash() const
{
    return m_hash;
}

QHash<uchar, QString> Tree::buildHash(Node *aNode, QString temp) {
    Q_ASSERT_X(m_root,"buildHash()", "Sorry. Not possible.");
    if(aNode->isLeaf()) {
        m_hash.insert(aNode->getSymbol(),temp);
        qDebug() << "Simbolo: " << aNode->getSymbol() << "\t"
                 << "Codificado: " << qPrintable(temp);
    } else {
        temp += '0';
        buildHash(aNode->getLeft(), temp);
        temp = temp.mid(0, temp.length() - 1);
        temp += '1';
        buildHash(aNode->getRight(), temp);
    }
    return m_hash;
}

void Tree::printTree(Node *aNode, int level)
{
    if(aNode != 0){
        printTree(aNode->getRight(), level + 1);
        if(aNode->isLeaf())
            qDebug() << qPrintable(QString("\t").repeated(level))
                     << char(aNode->getSymbol()) << "/" << aNode->getContent();
        else
            qDebug() << qPrintable(QString("\t").repeated(level)) << char(46);
        printTree(aNode->getLeft(), level + 1);
    }
}

void Tree::printHash(Tree*& m_tree)
{
    QHash<uchar, QString>::const_iterator count;
    for(count = m_tree->getHash().constBegin();
        count != m_tree->getHash().constEnd(); ++count) {
        qDebug() << char(count.key()) << ": " << qPrintable(count.value()) << endl;
    }
}

void Tree::printRepresentation(Tree *& m_tree)
{
    qDebug() << "Tree representation: " << endl
             << qPrintable(m_tree->getRepresentation());
}

QByteArray Tree::getRepresentation() const
{
    return m_representation;
}

